FROM nvcr.io/nvidia/pytorch:20.10-py3
RUN apt-get update && apt-get upgrade -y
RUN python3 -m pip uninstall -y \
    tensorboard \
    tensorboard-plugin-dlprof \
    nvidia-tensorboard \
    nvidia-tensorboard-plugin-dlprof \
    jupyter-tensorboard \
    && \
python3 -m pip --no-cache-dir install --upgrade \
    tensorboard==2.0.0 \
    omegaconf==2.1.0 \
    gpustat==0.6.0 \
    grpcio==1.13.0 \
    grpcio-tools==1.13.0 \
    protobuf==3.6.0 \
    Cython==0.29.21 \
    librosa==0.8.0 \
    matplotlib==3.3.1 \
    numpy==1.18.5 \
    scipy==1.5.2 \
    Unidecode==1.1.1 \
    && \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu && \
apt clean && \
apt autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/* && \
mkdir /root/brain_multilingual_vits
COPY . /root/brain_multilingual_vits
RUN cd /root/brain_multilingual_vits/monotonic_align && \
python3 setup.py build_ext --inplace
WORKDIR /root/brain_multilingual_vits