# VITS Multilingual TTS

Kakao의 [**Conditional Variational Autoencoder with Adversarial Learning for End-to-End Text-to-Speech**](https://arxiv.org/abs/2106.06103)을 기반으로 한 Multilingual TTS입니다.   
학습 메뉴얼은 [confluence](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=43115090)에 있습니다. 아래는 간단한 요약이니 자세한 내용을 보려면 confluence를 참고해주세요.   

## Start Learning

### Docker
Dockerfile을 build한 다음 컨테이너를 만들고 실행해주세요.   
학습 전 configuration file과 metadata를 확인/작성하세요.   
Dockerfile를 이용하지 않았다면 monotonic_align에 들어가서 아래의 코드를 입력해줘야 합니다.   
```sh
cd monotonic_align
python setup.py build_ext --inplace
```

### G2P
현재 사용되는 네 언어의 g2p는 아래와 같습니다.   
filelists 폴더에 있는 metadata들은 이미 g2p가 되어 있습니다.   
[korean_g2p](https://pms.maum.ai/confluence/display/audio/rbkog2p%3A+Rule-based+Korean+G2P)   
[english_g2p](https://pms.maum.ai/confluence/display/audio/English+G2P)   
[japanese_g2p](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=27951029), [japanese_g2p_new](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=43110951)   
[chinese_g2p](https://github.com/mozillazg/python-pinyin)


### Preprocess
`wav_path|text|speaker` 형태로 된 metadata를 사용합니다.   
`text`는 모두 g2p를 거친 상태여야 합니다. 언어마다 텍스트 형식이 다르기 때문에 이에 유의하여 바꿔주세요.   
speaker는 str 형식으로 적혀있으며 모든 speaker 이름들은 config파일의 data.speakers에 연달아 적어야 합니다.   
같은 언어를 사용하는 speaker라면 config파일의 data.speakers에 적을때 연달아 붙여서 적어야 합니다.   

### Configuration File
YAML 파일의 data 아래 n_speakers에는 speaker의 총 수를 적습니다.   
언어당 화자수를 data의 n_speakers_per_language에 적습니다.   
   
ex) 한국어화자가 27명, 영어화자가 161명, 일본어화자가 110명, 중국어화자가 174명이면 n_speakers는 이들의 총 합인 472가 되고 n_speakers_per_language에는 [27, 161, 110, 174]를 적으시면 됩니다. config파일의 data.speakers에 speaker들의 이름을 적을 때 처음부터 27번째까지의 화자는 모두 한국어 화자여야 하고 영어 화자는 28번부터 188번까지의 구간에 적어주셔야 합니다.   
   
data_path에는 audio data가 존재하는 path를, training_files, validation_files에는 g2p 처리를 거친 metadata의 위치를 적어주시면 됩니다.
모델의 체크포인트와 로그 등은 log_path에 저장됩니다.   
balance_sampling을 True로 설정하면 speaker와 language에 대해서 동등한 양의 text가 훈련에 사용됩니다. (많은 text를 가지고 있는 speaker, language는 훈련에 사용되는 빈도가 줄어들어 모든 speaker, language에 대하여 훈련에 사용될 확률이 동일해집니다.)   


### Training
```sh
python3 -W ignore train_ms.py -c CONFIG_PATH -m MODEL_NAME --ignore_warning
```
-W ignore와 --ignore_warning는 오류 메세지가 안 보이게 만듭니다.   
가지고 있는 체크포인트에서 계속해서 훈련을 이어가고 싶다면 -r <checkpoint 경로>를 추가하시면 됩니다.   

### Inference
`inference.ipynb`를 jupyter notebook으로 열고 체크포인트, text, spaeker, language를 정한 다음 실행시키면 음성을 합성하여 들을 수 있습니다.   
net_g.infer함수에서 화자의 원래 언어와 말하는 text의 언어가 다르다면 language인자를 추가해줘야 합니다. lan_speakers에 적혀 있는 언어의 순서대로 0,1,...이 되고 text의 언어에 맞게 이 값을 language인자에 적어주시면 됩니다. language인자를 추가하면 duration predictor에서 speaker의 정보를 사용하지 않게됩니다.   


## Pretrained model
이미 훈련된 모델을 제공하고 있습니다.v100 gpu 두개로 약 11일동안 훈련한 결과입니다. (balance_sampling을 False로 설정했습니다.)   
[google drive](https://drive.google.com/file/d/1EwLKvHpesxmlyjfTw3W9BMa1xSfjuRs0/view?usp=sharing)   
에 있는 파일을 다운로드 후 inference.ipynb를 jupyter notebook으로 실행해서 pretrained model로 inference할 수 있습니다.   

## Others

### TensorBoard
```sh
tensorboard --port=6006 --logdir path/to/log_dir --bind_all
```

### Jupyter Notebook
```sh
jupyter notebook --port=6006 --allow-root
```
