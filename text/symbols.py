from itertools import accumulate

_pad = '_'
_punc = ';:,.!?¡¿—-…«»“”~()" '+"'"

_jamo_leads = "".join([chr(_) for _ in range(0x1100, 0x1113)])
_jamo_vowels = "".join([chr(_) for _ in range(0x1161, 0x1176)])
_jamo_tails = "".join([chr(_) for _ in range(0x11A8, 0x11C3)])
_kor_characters = _jamo_leads + _jamo_vowels + _jamo_tails

_cmu_characters = [
    'AA', 'AE', 'AH',
    'AO', 'AW', 'AY',
    'B', 'CH', 'D', 'DH', 'EH', 'ER', 'EY',
    'F', 'G', 'HH', 'IH', 'IY',
    'JH', 'K', 'L', 'M', 'N', 'NG', 'OW', 'OY',
    'P', 'R', 'S', 'SH', 'T', 'TH', 'UH', 'UW',
    'V', 'W', 'Y', 'Z', 'ZH'
]

_jap_romaji_characters = [
    'N', 'a', 'b', 'by', 'ch', 'cl', 'd', 'dy', 'e', 'f', 'g',
    'gy', 'h', 'hy', 'i', 'j', 'k', 'ky', 'm', 'my', 'n', 'ny',
    'o', 'p', 'pau', 'py', 'r', 'ry', 's', 'sh', 't', 'ts', 'u',
    'v', 'w', 'y', 'z'
]

_pinyin_characters = [
    'b', 'p', 'm', 'f', 'd', 't', 'n', 'l', 'g', 'k', 'h', 'j', 'q', 'x', 'z', 'c', 's', 'zh', 'ch', 'sh', 'r', 'y', 'w', 
    'a', 'o', 'e', 'i', 'u', 'v', 
    'ai', 'ei', 'ui', 
    'ao', 
    'ou', 'iu', 
    'ie', 've', 'ue', 
    'er', 
    'an', 'en', 'in', 'un', 
    'ang', 'eng', 'ing', 'ong', 
    'ia', 'iao', 'ian', 'iang', 
    'ua', 'uan', 'uang', 'uo', 'iong', 'uai'
]
_pinyin_other = '%$'

lang_to_symbols = {
    'common': [_pad] + list(_punc),
    'ko_KR': list(_kor_characters), 
    'en_US': _cmu_characters, 
    'ja_JP': _jap_romaji_characters, 
    'zh_CN': _pinyin_characters + list(_pinyin_other),
}

def lang_to_dict(lang_all, lang_select):
    all_symbol = lang_to_symbols['common'] + [symbol for lang in lang_all for symbol in lang_to_symbols[lang]]
    len_list = [len(lang_to_symbols['common'])] + [len(lang_to_symbols[lang]) for lang in lang_all]
    index = lang_all.index(lang_select)
    len_list = list(accumulate(len_list))
    dict_lang = {s: i for i, s in enumerate(all_symbol) if (i < len_list[0]) or (i >= len_list[index] and i < len_list[index + 1])}
    return dict_lang

def symbol_len(lang_all):
    all_symbol = lang_to_symbols['common'] + [symbol for lang in lang_all for symbol in lang_to_symbols[lang]]
    return len(all_symbol)
