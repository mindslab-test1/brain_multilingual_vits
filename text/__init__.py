""" from https://github.com/keithito/tacotron """
import re

from text import cleaners
from text.symbols import lang_to_dict


def text_to_sequence(raw_text, lang_all, text_cleaners, lang=None):
    '''Converts a string of text to a sequence of IDs corresponding to the symbols in the text.
    Args:
        text: string to convert to a sequence
        cleaner_names: names of the cleaner functions to run the text through
    Returns:
        List of integers corresponding to the symbols in the text
    '''
    
    _symbol_to_id = lang_to_dict(lang_all, lang)
    cleaner_name = text_cleaners[lang_all.index(lang)]
    text = _clean_text(raw_text, cleaner_name)
    
    if lang == 'ko_KR':
        sequence = [_symbol_to_id[symbol] for symbol in text]
        tone = [0 for i in sequence]
    elif lang == 'en_US':
        _curly_re = re.compile(r'(.*?)\{(.+?)\}(.*)')
        sequence = []
        while len(text):
            m = _curly_re.match(text)
            if m is not None:
                ar = m.group(1)
                sequence += [_symbol_to_id[symbol] for symbol in ar]
                ar = m.group(2)
                sequence += [_symbol_to_id[symbol] for symbol in ar.split()]
                text = m.group(3)
            else:
                sequence += [_symbol_to_id[symbol] for symbol in text]
                break
        tone = [0 for i in sequence]
    elif lang == 'ja_JP':
        text = text.split('-')
        sequence = [_symbol_to_id[symbol] for symbol in text]
        tone = [0 for i in sequence]
    elif lang == 'zh_CN':
        vowel = ['a', 'e', 'o', 'i', 'u', 'v']
        sequence = []
        tone = []
        text = text.split(" ")
        for symbol in text:
            if symbol in ['$', '%']:
                sequence.append(_symbol_to_id[symbol])
                tone.append(0)
                continue
            symbol_tone = int(symbol[-1])
            len_of_tone = 0

            if symbol[-2] == 'r':
                symbol = symbol[:-2]
                er = True
            else:
                symbol = symbol[:-1]
                er = False

            if symbol[0] in vowel:
                sequence.append(_symbol_to_id[symbol])
                len_of_tone += 1
            elif symbol[1] in vowel:
                sequence.append(_symbol_to_id[symbol[0:1]])
                sequence.append(_symbol_to_id[symbol[1:]])
                len_of_tone += 2
            elif symbol[2] in vowel:
                sequence.append(_symbol_to_id[symbol[0:2]])
                sequence.append(_symbol_to_id[symbol[2:]])
                len_of_tone += 2
            else:
                raise RuntimeError('no vowel')

            if er:
                sequence.append(_symbol_to_id['er'])
                len_of_tone += 1
            tone = tone + [symbol_tone for i in range(len_of_tone)]
    else:
        raise RuntimeError('Wrong type of lang')
    assert len(sequence) == len(tone)
    return sequence, tone

def _clean_text(raw_text, cleaner_names):
    cleaner = getattr(cleaners, cleaner_names)
    if not cleaner:
        raise Exception('Unknown cleaner: %s' % cleaner_names)
    text = cleaner(raw_text)
    return text