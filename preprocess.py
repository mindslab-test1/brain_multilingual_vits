import os.path
import argparse
from tqdm import tqdm
from pypinyin import lazy_pinyin, Style

from utils import load_filepaths_and_text

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--out_extension", default="number")
    parser.add_argument("--text_index", default=2, type=int)
    parser.add_argument(
        "--filelists", 
        nargs="+", 
        default=["filelists/text_val_filelist.txt", "filelists/text_text_test_filelist.txt"]
    )

    args = parser.parse_args()
    speaker = dict()
    speaker_id = 297

    for filelist in args.filelists:
        print("START:", filelist)
        filepaths_and_text = load_filepaths_and_text(filelist)
        for i in tqdm(range(len(filepaths_and_text))):
            abc = filepaths_and_text[i][0]
            hanja = filepaths_and_text[i][2]
            filepaths_and_text[i][0] = "aishell_3_wav/"+ abc[:7] + "/" + abc+".wav"
            original_text = abc[:7]
            if original_text in speaker:
                filepaths_and_text[i][2] = str(speaker[original_text])
            else:
                filepaths_and_text[i][2] = str(speaker_id)
                speaker[original_text] = speaker_id
                speaker_id = speaker_id + 1
            filepaths_and_text[i][1] = lazy_pinyin(hanja, style=Style.TONE3, tone_sandhi=True, neutral_tone_with_five=True)

        new_filelist = filelist + "_" + args.out_extension
        speaker_id_path = os.path.dirname(filelist) + "/speaker_id.txt"
        with open(new_filelist, "w", encoding="utf-8") as f:
            f.writelines([x[0]+"|"+' '.join(x[1])+"|"+x[2] + "\n" for x in filepaths_and_text])
    with open(speaker_id_path, "w", encoding="utf-8") as f:
        f.writelines([str(y) + "\t" + x + "\n" for x, y in speaker.items()])
